import unittest
from break_repeating_key_XOR import *

"""
1. Calculate de Humming distance of two strings
2. Estimate the KEYSIZE betwen 2 and 40 and
3. Estimate the Huming Distance between pairs of KEYSIZE lenght and normalize
4. Get the smallest KEYSIZE result and break the chipher text into blocks of KEYSIZE lenght
"""


class BreakingRepeatingXorTest(unittest.TestCase):
    def setUp(self):
        self.string = b"abcdeabcfg"
        self.value_keysize_1 = 1
        self.value_keysize_2 = 5
        self.check_string_1 = b"this is a test"
        self.check_string_2 = b"wokka wokka!!!"

    def test_hamming_distance(self):
        self.assertEqual(hamming_distance(self.check_string_1, self.check_string_2), 37)
        self.assertEqual(
            hamming_distance(self.check_string_1[:2], self.check_string_1[2:4]), 8
        )

    def test_hamming_distance_of_each_block(self):
        self.assertEqual(hamming_distance_first_and_second_worth_of_bytes(b"az", 1), 4)

    def test_hamming_distance_normalized_for_KEYSIZE(self):
        self.assertEqual(
            hamming_distance_normalized_for_KEYSIZE(self.check_string_1, 3),
            1.6666666666666667,
        )

    def test_smallest_normalized_edit_distance(self):
        self.assertEqual(
            smallest_normalized_edit_distance_key(
                self.string, self.value_keysize_1, self.value_keysize_2
            ),
            5,
        )

    def test_break_the_text_into_blocks_of_keysize_length(self):
        self.assertEqual(
            break_the_text_into_blocks_of_keysize_length(
                self.string, self.value_keysize_1, self.value_keysize_2
            ),
            [b"abcde", b"abcfg"],
        )
        self.assertEqual(
            break_the_text_into_blocks_of_keysize_length(
                self.check_string_1, self.value_keysize_1, self.value_keysize_2
            ),
            [b"thi", b"s i", b"s a", b" te", b"st"],
        )

    def test_transpose_bytes(self):
        self.assertEqual(
            transpose_bytes_from_brack_text(
                self.check_string_1, self.value_keysize_1, self.value_keysize_2
            ),
            [b"tss s", b"h  tt", b"iiae"],
        )


if __name__ == "__main__":
    unittest.main()
