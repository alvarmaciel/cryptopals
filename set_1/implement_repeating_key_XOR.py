"""
 Here is the opening stanza of an important work of the English language:

Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal

Encrypt it, under the key "ICE", using repeating-key XOR.

In repeating-key XOR, you'll sequentially apply each byte of the key; the first byte of plaintext will be XOR'd against I,
the next C, the next E, then I again for the 4th byte, and so on.

It should come out to:



0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272
a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f

Encrypt a bunch of stuff using your repeating-key XOR function. Encrypt your mail. Encrypt your password file.
Your .sig file. Get a feel for it. I promise, we aren't wasting your time with this.
"""


def xor_str_against_str_key(message: str, key: str) -> str:
    index_key = 0
    coded_message = ""
    for bytes in message:
        if index_key >= len(key):
            index_key = 0

        result = xor_str_to_char(bytes, key[index_key])
        coded_message += result
        index_key += 1

    return coded_message


def xor_char_to_char(first_char: str, second_char: str) -> int:
    return ord(first_char) ^ ord(second_char)


def convert_str_to_byte(message: str) -> bytes:
    return message.encode('UTF-8')


def xor_str_to_char(message: str, key: str) -> str:
    message_bytes = convert_str_to_byte(message)
    key_byte = convert_str_to_byte(key)
    encoded_message = bytearray(_byte ^ ord(key_byte)
                                for _byte in message_bytes).hex()
    return encoded_message


if __name__ == '__main__':
    pass
