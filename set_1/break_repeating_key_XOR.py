"""There's a file here. It's been base64'd after being encrypted with repeating-key XOR. Decrypt it.

Here's how:

- [x]1. Let KEYSIZE be the guessed length of the key; try values from 2 to (say) 40.

- [x] 2. Write a function to compute the edit distance/Hamming distance between two strings. 
The Hamming distance is just the number of differing bits. The distance between:


    and

    wokka wokka!!!

    is 37. Make sure your code agrees before you proceed.

- [x] 3. For each KEYSIZE, take the first KEYSIZE worth of bytes, and the second
  KEYSIZE worth of bytes, and find the edit distance between them. Normalize
  this result by dividing by KEYSIZE.

- [x] 4. The KEYSIZE with the smallest normalized edit distance is probably the
key. You could proceed perhaps with the smallest 2-3 KEYSIZE values. Or take 4
KEYSIZE blocks instead of 2 and average the distances.

- [x] 5. Now that you probably know the KEYSIZE: break the ciphertext into
  blocks of KEYSIZE length.

- [ ] 6. Now transpose the blocks: make a block that is the first byte of every
  block, and a block that is the second byte of every block, and so on.

- [ ] 7. Solve each block as if it was single-character XOR. You already have
  code to do this.

- [ ] 8. For each block, the single-byte XOR key that produces the best looking
histogram is the repeating-key XOR key byte for that block. Put them together
and you have the key.

This code is going to turn out to be surprisingly useful later on. Breaking
repeating-key XOR ("Vigenere") statistically is obviously an academic exercise,
a "Crypto 101" thing. But more people "know how" to break it than can actually
break it, and a similar technique breaks something much more important.

"""
import base64
from implement_repeating_key_XOR import *


def transpose_bytes_from_brack_text(
    string: bytes, value_key_1: int, value_key_2
) -> bytes:
    """
    This function takes every first block of the blocks in what the stings are
    chunked by their normalized hamming distance and put them toghether, the
    same does with the second, third and so one
    """
    result = []
    list_to_process = break_the_text_into_blocks_of_keysize_length(
        string, value_key_1, value_key_2
    )
    j = 0
    for block in list_to_process:
        print(block)
        for i in range(len(block)):
            print(i)
            print(chr(block[i]))
            result[i].append(chr(block[i]))
            print(result)
            print("----")

    return result


def break_the_text_into_blocks_of_keysize_length(
    string: bytes, value_key_1: int, value_key_2: int
) -> bytes:
    blocks_of = smallest_normalized_edit_distance_key(string, value_key_1, value_key_2)
    return [string[i : i + blocks_of] for i in range(0, len(string), blocks_of)]


def smallest_normalized_edit_distance_key(
    string_to_process: bytes, value_keysize_1: int, value_keysize_2: int
) -> int:
    smallest_distance = hamming_distance(
        string_to_process, string_to_process[::-1]
    )  # defining the higer distance by compare the string with her inverse
    for i in range(value_keysize_1, value_keysize_2 + 1):
        hamming_distance_i = hamming_distance_normalized_for_KEYSIZE(
            string_to_process, i
        )
        if hamming_distance_i <= smallest_distance:
            smallest_distance = hamming_distance_i
            smallest_distance_key = i
    return smallest_distance_key


def hamming_distance_normalized_for_KEYSIZE(
    string_to_process: bytes, keysize: int
) -> int:

    return (
        hamming_distance_first_and_second_worth_of_bytes(string_to_process, keysize)
        / keysize
    )


def hamming_distance_first_and_second_worth_of_bytes(
    string_to_proces: bytes, keysize: int
) -> int:

    # half_of_string_to_process = len(string_to_proces) // 2

    return hamming_distance(
        string_to_proces[:keysize],
        string_to_proces[keysize : keysize * 2],
    )


def hamming_distance(value_1: bytes, value_2: bytes) -> int:

    final_answer_count = 0
    for i in range(len(value_1)):
        a = value_1[i]
        b = value_2[i]
        xor_chars = a ^ b

        while xor_chars > 0:
            final_answer_count += xor_chars & 1

            xor_chars = xor_chars >> 1

    return final_answer_count


if __name__ == "__main__":
    file = open("6.txt", "r")
    base64_message = file.read()
    base64_bytes = base64_message.encode("ascii")
    message_bytes = base64.b64decode(base64_bytes)
    print(break_the_text_into_blocks_of_keysize_length(message_bytes, 2, 40))
