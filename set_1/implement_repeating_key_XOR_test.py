import unittest
from implement_repeating_key_XOR import *
"""
test:
1. function xor against chars test result in integer and later
2. cshow result as hex
2. function that map a char of a string against another char of a string and make xor
"""


class XorsTest(unittest.TestCase):
    def test_xor_against_one_char(self):
        self.assertEqual(xor_char_to_char("A", "B"), 3)

    def test_convert_str_to_byte(self):
        self.assertEqual(convert_str_to_byte("AA"), b'AA')

    def test_xor_string_against_one_char(self):
        self.assertEqual(xor_str_to_char("AA", "B"), "0303")

    def test_xor_string_against_str_key(self):
        self.assertEquals(xor_str_against_str_key("Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal", "ICE"),
                          "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f")


if __name__ == '__main__':
    unittest.main()
