# Convert hex to base64
import base64

"""
The string:

49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d

Should produce:

SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t

So go ahead and make that happen. You'll need to use this code for the rest of the exercises.

Cryptopals Rule

Always operate on raw bytes, never on encoded strings. Only use hex and base64 for pretty-printing.
Tareas:

 - ¿Como cnvertirlo en un módulo https://www.ictshore.com/python/create-python-modules-tutorial/
 - ¿Cómo correrle test?
"""


def hex_to_bytes(string_hex):
    
    bytes_data = bytes.fromhex(string_hex)
    #base64_code = base64.b64encode(bytes_data)
    return bytes_data


def hex_to_int(string_hex):
    bytes_data = bytes.fromhex(string_hex)
    int_code = int.from_bytes(bytes_data, "big")
    return int_code
