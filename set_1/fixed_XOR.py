"""
 Write a function that takes two equal-length buffers and produces their XOR combination.

If your function works properly, then when you feed it the string:

1c0111001f010100061a024b53535009181c

... after hex decoding, and when XOR'd against:

686974207468652062756c6c277320657965

... should produce:

746865206b696420646f6e277420706c6179
"""
import hex_to_x as hx  ## Mi múdulo !!!

def xor_combination(buff1, buff2):
    result = buff1 ^ buff2
    return result


if __name__ == "__main__":
    buff1 = hx.hex_to_int(
        "1c0111001f010100061a024b53535009181c"
    )  ## Llamo a mi módulo !!!
    buff2 = hx.hex_to_int(
        "686974207468652062756c6c277320657965"
    )  ## Llamada a mi modulo
    result = format(xor_combination(buff1, buff2), "x")
    # result = format(result, "x")
    print(result)
