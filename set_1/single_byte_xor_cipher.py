import base64

"""
 The hex encoded string:

1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736

... has been XOR'd against a single character. Find the key, decrypt the message.

You can do this by hand. But don't: write code to do it for you.

How? Devise some method for "scoring" a piece of English plaintext. Character frequency is a good metric. Evaluate each output and choose the one with the best score.
"""

# 1. xor el string contra cada caracter del la tabla ascii
# convertir el string en int
# meter el string en un loop que xorea contra int de 1 a 127 de la tabla ascci  y guarda la salida en una lista
# 2. pasar los resultados evaluando la frecuencia de caracteres http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html


def set_score_by_frecuency(decoded_string: list[bytes]) -> int:
    score = 0
    frecuency_letters = {
        "e": 13.0,
        "t": 9.10,
        "a": 8.2,
        "o": 7.5,
        "i": 7.0,
        "n": 6.7,
        "s": 6.3,
        "r": 6.00,
        "h": 6.1,
        "d": 4.3,
        "l": 4.0,
        "u": 2.8,
        "c": 2.8,
        "m": 2.4,
        "f": 2.2,
        "y": 2.0,
        "w": 2.4,
        "g": 2.00,
        "p": 1.9,
        "b": 1.5,
        "v": 0.98,
        "k": 0.77,
        "x": 0.15,
        "q": 0.095,
        "j": 0.15,
        "z": 0.074,
        "`": -100,
        "^": -100,
        "~": -100,
    }
    letters_in_string = set(frecuency_letters).intersection(decoded_string)
    # puntaje = sum(frecuencia_letras[k] for k in letras_coincidentes)
    for k in letters_in_string:
        score = score + frecuency_letters[k]
    return score


def set_max_score_by_frecuency(decoded_list: list[list[bytes]]) -> bytes:
    max_score = 0
    score = int()
    decoded_string = ""
    for i in decoded_list:
        score = set_score_by_frecuency(i)
        if score > max_score:
            max_score = score
            best_decoded_string = i
    decoded_string = decoded_string.join(best_decoded_string)
    decoded_string_bytes = bytes(decoded_string, "utf-8")
    # decoded_string_bytes64=base64.b64encode(decoded_string_bytes)
    return decoded_string_bytes


def xor_against_ascii_chars(frase_cifrada: list[bytes], key: int) -> list[list[bytes]]:
    """
    Function that makes XOR between:
    hex string mesaje converted to bytes
    and
    a single char converted to byte
    """
    decoded_list = []

    for i in range(key):
        decode = [chr(_a ^ i) for _a in frase_cifrada]
        decoded_list.append(decode)
    return decoded_list


if __name__ == "__main__":
    mensaje = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    mensaje_bytes = bytes.fromhex(mensaje)
    key = 256
    lista_mejor_punteada = set_max_score_by_frecuency(
        xor_against_ascii_chars(mensaje_bytes, key)
    )
    print(f"Encoded message: {mensaje}")
    print(f"Byte message: {mensaje_bytes}")
    print(f"Decoded  message: {lista_mejor_punteada}")
