import single_byte_xor_cipher as sbxc

"""
 One of the 60-character strings in this file has been encrypted by single-character XOR.
file: https://cryptopals.com/static/challenge-data/4.txt
Find it.

(Your code from #3 should help.) 
1. Open File
2. convert every hex string to bytes
3. send every line of file to xor_against_ascii_chars and save it in a list
3. Send list to set_max_score_by_frecuency to find the legible line
4. print line
"""


def decode_list_bytes(encoded_list_bytes: list[bytes]) -> list[bytes]:
    decoded_list = []
    for i in encoded_list_bytes:
        decoded_line = sbxc.set_max_score_by_frecuency(
            sbxc.xor_against_ascii_chars(i, 256)
        )
        decoded_list.append(decoded_line)

    return decoded_list


def convet_to_list(decoded_list: list[bytes]) -> list[list[bytes]]:
    splited_list = list(decoded_list)
    splited_list_bytes = [chr(i) for i in splited_list]
    return splited_list_bytes


if __name__ == "__main__":
    file = open("4.txt", "r")
    encoded_list_hex = file.readlines()
    # read_file = file.read()
    encoded_list_bytes = [bytes.fromhex(i) for i in encoded_list_hex]
    decoded_list = decode_list_bytes(encoded_list_bytes)
    posible_answer = [convet_to_list(i) for i in decoded_list]
    answer = sbxc.set_max_score_by_frecuency(posible_answer)
    print(answer)
